Unified Device Tree for Lenovo Vibe K5 Note (A7020A48)
======================================================

The Lenovo Vibe K5 Note (codenamed _"A7020A48"_) is a mid-range smartphone from Lenovo mobility.

Basic   | Spec Sheet
-------:|:-------------------------
CPU     | Quad-core 1.5 GHz Cortex-A53 & quad-core 1.2 GHz Cortex-A53
Chipset | Qualcomm MSM8929 Snapdragon 415
GPU     | Adreno 405
Memory  | 3/4 GB RAM
Shipped Android Version | 5.1
Storage | 32 GB
MicroSD | Up to 32 GB
Battery | Removable Li-Ion 2750mAh battery
Display | 720 x 1280 pixels, 5.0 inches (~294 ppi pixel density)
Camera  | 13 MP, autofocus, flash LED

Copyright 2017 - The LineageOS Project.

![Lenovo Vibe K5](http://cdn2.gsmarena.com/vv/bigpic/lenovo-k5.jpg "Lenovo Vibe K5 Note")

Supported variants
------------------
Variant | Codename
-------:|:--------

